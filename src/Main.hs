-- regexp-viz - LPP 2010
-- Piotr Bober
-- main file

module Main where

-- my modules
import Parser (parser)
import Converter (convert)
import DFA (regexp_to_dfa, regexp_to_ndfa, ndfa_to_dfa, optimize, min_dfa, DFA, init, delta)
import Graph (dfa_to_graph, dfa_to_graph_color)
-- system modules
import System.Process (system)
import System.IO.UTF8 (writeFile, putStrLn)
import Prelude hiding (writeFile, putStrLn, init)
import Graphics.UI.WX

-- MAIN

-- gui main
main :: IO ()
main = start regexpviz

-- main frame
regexpviz :: IO ()
regexpviz = do
   -- variables
   dfa_var <- varCreate min_dfa
   visited_states <- varCreate []
   -- frame
   f <- frameFixed [text := "regexp-viz"]
   -- input for reg exp
   re_input_panel <- panel f []
   re_input <- entry re_input_panel []
   generate_button <- button re_input_panel [text := "Generate"]
   -- dfa picture
   dfa <- panel f [size := sz 800 600]
   -- control panel
   control <- panel f []
   word_input <- entry control []
   state_input <- staticText control []
   next_button <- button control [text := "Next letter", on command := nextLetter dfa word_input state_input dfa_var visited_states]
   all_button <- button control [text := "Read all", on command := readWord word_input state_input dfa dfa_var visited_states]
   reset_button <- button control [text := "Reset", on command := reset_dfa dfa state_input visited_states dfa_var]
   -- set actions
   set generate_button [on command := buildDFA dfa_var visited_states state_input re_input dfa >> focusOn word_input]
   set re_input [on (key KeyReturn) := buildDFA dfa_var visited_states state_input re_input dfa >> focusOn word_input]
   set word_input [on (key KeyReturn) := nextLetter dfa word_input state_input dfa_var visited_states]
   -- set layout
   set re_input_panel [layout := margin 5 $ row 5 $ map centre [
      hspace 50,
      label "Regular expression:",
      hfill $ widget re_input,
      widget generate_button,
      hspace 80]]
   set control [layout := margin 5 $ boxed "Control Panel" $
      column 5 [
         row 5 $ map centre [
            hspace 100,
            label "Input word:",
            hfill $ widget word_input,
            hspace 100],
         row 5 [
            hspace 120,
            floatRight $ label "Current state:",
            floatLeft $ widget state_input,
            floatCentre $ widget next_button,
            floatCentre $ widget all_button,
            floatCentre $ widget reset_button,
            hspace 120]
      ]]
   set f [layout := column 0 $ map centre [
      hfill $ widget re_input_panel,
      widget dfa,
      hfill $ widget control]]
   focusOn re_input

-- function that draws the dfa
paintDFA :: String -> DC a -> Rect -> IO ()
paintDFA file dc _ = do
   putStrLn "Repainting"
   drawImage dc (image file) pointZero []

-- build DFA
buildDFA :: Var DFA -> Var [Int] -> StaticText () -> TextCtrl () -> Panel () -> IO ()
buildDFA vdfa visited_states txt re_input dfa_panel = do
   regexp <- get re_input text
   case parser regexp of
      Left e ->
         errorDialog dfa_panel "Syntax error" $ "parse error at " ++ show e
      Right x -> do
         let re = convert x
         let ndfa = regexp_to_ndfa re
         let dfa = ndfa_to_dfa ndfa
         let automaton = optimize dfa
         --let automaton = regexp_to_dfa $ convert ere
         -- debug info (step by step)
         putStrLn "---------------------------------------------------------------"
         putStrLn "Regular Expression:"
         print regexp
         putStrLn "---------------------------------------------------------------"
         putStrLn "Abstract Syntax:"
         print re
         putStrLn "---------------------------------------------------------------"
         putStrLn "NDFA:"
         print ndfa
        -- putStrLn "---------------------------------------------------------------"
        -- putStrLn "DFA:"
        -- print dfa
         putStrLn "---------------------------------------------------------------"
         putStrLn "Optimized DFA:"
         print automaton
         putStrLn "---------------------------------------------------------------"
         -- end debug info
         varSet vdfa automaton
         let q0 = init automaton
         varSet visited_states [q0]
         set txt [text := show q0]
         repaint_dfa dfa_panel vdfa visited_states

-- DFA word processing functions
nextLetter :: Panel () -> TextCtrl () -> StaticText () -> Var DFA -> Var [Int] -> IO ()
nextLetter dfa_panel word_field state_field vdfa vstates = do
   word <- get word_field text
   case word of
      "" -> errorDialog dfa_panel "Error" "There are no more letters to process"
      _ ->
         do
            state <- get state_field text
            dfa <- varGet vdfa
            let new_state = delta dfa (read state :: Int) (head word)
            set state_field [text := show new_state]
            set word_field [text := tail word]
            states <- varGet vstates
            varSet vstates $ states ++ [new_state]
            repaint_dfa dfa_panel vdfa vstates

readWord :: TextCtrl () -> StaticText () -> Panel () -> Var DFA -> Var [Int] -> IO ()
readWord word_field state_field dfa_panel vdfa vstates =  do
   word <- get word_field text
   state <- get state_field text
   dfa <- varGet vdfa
   states <- varGet vstates
   let visited = scanl (delta dfa) (read state :: Int) word
   set state_field [text := show $ last visited]
   set word_field [text := ""]
   varSet vstates $ states ++ visited
   repaint_dfa dfa_panel vdfa vstates

reset_dfa :: Panel () -> StaticText () -> Var [Int] -> Var DFA -> IO ()
reset_dfa dfa_panel state_input visited_states dfa_var = do
   dfa <- varGet dfa_var
   let q0 = init dfa
   varSet visited_states [q0]
   set state_input [text := show q0]
   repaint_dfa dfa_panel dfa_var visited_states

-- dfa image repainting
repaint_dfa :: Panel () -> Var DFA -> Var [Int] -> IO ()
repaint_dfa dfa_panel dfa_var states_var = do
   dfa <- varGet dfa_var
   states <- varGet states_var
   graph "graph" $ dfa_to_graph_color dfa states
   set dfa_panel [on paint := paintDFA "graph.png"]
   repaint dfa_panel

-- write a file with the DOT code
graph :: String -> String -> IO ()
graph name code = do
   writeFile (name ++ ".dot") code
   system ("dot -Tpng " ++ name ++ ".dot -o " ++ name ++ ".png")
   putStrLn "Graph generated."

{---------------------------------------------------------------------------------------------------
-- GUI layout:

   //=================================================\\
   ||           [  input for regexp  ]   {create}     ||
   ||-------------------------------------------------||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                       DFA                       ||
   ||                                                 ||
   ||                     picture                     ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||                                                 ||
   ||-------------------------------------------------||
   ||        [       input for word        ]          ||
   ||       [state]   {next step}  {read all}         ||
   \\=================================================//

-}
