-- regexp-viz - LPP 2010
-- Piotr Bober
-- POSIX extended regular expression (ERE) parser

module Parser where

import Text.ParserCombinators.Parsec
import qualified Text.ParserCombinators.Parsec.Token as T
import Text.ParserCombinators.Parsec.Expr
import Control.Monad.Identity (Identity) -- just for the type signature for lexer

-- data types for regular expressions

-- extended regular expression
data ERE = Alt ERE ERE                 -- alternative
         | Concat ERE ERE              -- sequence
         | OrdChar Char                -- ordinary character
         | QuotedChar Char             -- escaped special character
         | Dot                         -- anything
         | Bracket BracketExpression   -- choice list
         | Dup ERE_Dup ERE             -- expression with quantity
   deriving Show

-- quantity symbol
data ERE_Dup = Star                                   -- none or more
              | Plus                                  -- one or more
              | QMark                                 -- none or one
              | DupCount DUP_COUNT                    -- exact quantity
              | DupCountCommaSep DUP_COUNT DUP_COUNT  -- quantity range (maximum value noe necessary)
   deriving Show

type DUP_COUNT = Integer

-- maximum quantity
ere_max_dup :: DUP_COUNT
ere_max_dup = 30

-- choice list
data BracketExpression = Matching [SingleExpression]      -- inclusive choice list
                       | NonMatching [SingleExpression]   -- exclusive choice list
   deriving Show

-- single element
data SingleExpression = Class String               -- character class
                      | CE COLL_ELEM               -- single element
                      | Range COLL_ELEM COLL_ELEM  -- exact range
                      | RangeStart COLL_ELEM       -- unended range
   deriving Show

-- ordinary or quoted character
type COLL_ELEM = Char


-----------------------------------------------------------------
-- SYNTAX
-----------------------------------------------------------------
-- source:
--    http://www.opengroup.org/onlinepubs/007908799/xbd/re.html
-----------------------------------------------------------------
-- removed features:
--   * anchors
--   * collating symbols
--   * equivalence classes
-----------------------------------------------------------------
--
-- %start  extended_reg_exp
--
-- %token  ORD_CHAR QUOTED_CHAR COLL_ELEM DUP_COUNT
--
-- %token  OpenColon ColonClose ClassName
--
--
-- extended_reg_exp   : ERE_branch
--                    | extended_reg_exp '|' ERE_branch
--                    ;
--
-- ERE_branch         : ERE_expression
--                    | ERE_branch ERE_expression
--                    ;
--
-- ERE_expression     : one_character_ERE
--                    | '(' extended_reg_exp ')'
--                    | ERE_expression ERE_dup_symbol
--                    ;
--
-- one_character_ERE  : ORD_CHAR
--                    | QUOTED_CHAR
--                    | '.'
--                    | bracket_expression
--                    ;
--
-- ERE_dup_symbol    : '*'
--                    | '+'
--                    | '?'
--                    | '{' DUP_COUNT '}'
--                    | '{' DUP_COUNT ',' '}'
--                    | '{' DUP_COUNT ',' DUP_COUNT '}'
--                    ;
--
-- bracket_expression : '[' matching_list ']'
--                    | '[' nonmatching_list ']'
--                    ;
--
-- matching_list      : bracket_list
--                    ;
--
-- nonmatching_list   : '^' bracket_list
--                    ;
--
-- bracket_list       : expression_term
--                    | bracket_list expression_term
--                    ;
--
-- expression_term    : COLL_ELEM
--                    | character_class
--                    | range_expression
--                    ;
--
-- character_class    : OpenColon ClassName ColonClose
--                    ;
--
-- range_expression   : COLL_ELEM '-' COLL_ELEM
--                    | COLL_ELEM '-'
--                    ;
--
-----------------------------------------------------

----------------- P A R S E R S ---------------------

-----------------------------------------------------

-- language definition

reservedNames :: [String]
reservedNames = ["alnum", "alpha", "blank", "cntrl", "digit", "graph",
                 "lower", "print", "punct", "space", "upper", "xdigit"]

ereLangDef :: T.LanguageDef st
ereLangDef = T.LanguageDef {
   T.commentStart = "",
   T.commentEnd = "",
   T.commentLine = "",
   T.nestedComments = False,
   T.identStart = oneOf "",
   T.identLetter = oneOf "",
   T.opStart = oneOf "*+?  |",
   T.opLetter = oneOf "",
   T.reservedNames = reservedNames,
   T.reservedOpNames = ["|","*","+","?"],
   T.caseSensitive = True}

lexer :: T.GenTokenParser String u Identity
lexer = T.makeTokenParser ereLangDef

-- tokens

ordChar :: Parser ERE
ordChar = (noneOf ".[]()|*+?\\") >>= return . OrdChar

quotedChar :: Parser ERE
quotedChar =
   choice $ map (\x -> try (symbol x) >>= return . QuotedChar . last)
                ["\\.", "\\[", "\\]", "\\(", "\\)", "\\|", "\\*", "\\+", "\\?", "\\\\"]

collElem :: Parser COLL_ELEM
collElem = choice $
   (noneOf ".[]()|*+?\\" <?> "collating ordinary element") :
      map (\x -> try (symbol x) >>= return . last <?> "collating quoted element")
          ["\\.", "\\[", "\\]", "\\(", "\\)", "\\|", "\\*", "\\+", "\\?", "\\\\"]

dupCount :: Parser DUP_COUNT
dupCount = T.natural lexer

reserved :: String -> Parser ()
reserved = T.reserved lexer

className :: Parser String
className = (choice $ map (\x -> reserved x >> return x) reservedNames) <?> "class name"

squares :: Parser a -> Parser a
squares = T.squares lexer

braces :: Parser a -> Parser a
braces = T.braces lexer

parens :: Parser a -> Parser a
parens = T.parens lexer

symbol :: String -> Parser String
symbol = T.symbol lexer

reservedOp :: String -> Parser ()
reservedOp = T.reservedOp lexer

dot :: Parser String
dot = T.dot lexer

-- combinators

ere :: Parser ERE
ere = buildExpressionParser opTable ereSingle

opTable :: [[Operator Char () ERE]]
opTable = [
   [Postfix $ ereDupSymbol >>= \d -> return $ \e -> Dup d e],
   [Infix (return Concat) AssocLeft],
   [Infix (reservedOp "|" >> return Alt) AssocLeft]]

ereSingle :: Parser ERE
ereSingle =
   (quotedChar <?> "quoted character")
 <|>
   (ordChar <?> "ordinary character")
 <|>
   (dot >> return Dot <?> "dot")
 <|>
   (bracketExpr >>= return . Bracket)
 <|>
   (parens ere <?> "parenthesis")

ereDupSymbol :: Parser ERE_Dup
ereDupSymbol =
   do
      reservedOp "*"
      return Star
 <|>
   do
      reservedOp "+"
      return Plus
 <|>
   do
      reservedOp "?"
      return QMark
 <|>
   try (braces dupCount >>= return  .  DupCount)
 <|>
   try
      (braces (do
         d <- dupCount
         symbol ","
         return $ DupCountCommaSep d ere_max_dup)
      )
 <|>
   braces (do
      d1 <- dupCount
      symbol ","
      d2 <- dupCount
      return $ DupCountCommaSep d1 d2)
 <?> "duplicating symbol"

bracketExpr :: Parser BracketExpression
bracketExpr =
   try (squares (symbol "^" >> many1 single) >>= return . NonMatching)
 <|>
   (squares (many1 single) >>= return . Matching)
 <?> "bracket expression"

single :: Parser SingleExpression
single =
   try (do
           symbol "[:"
           c <- className
           symbol ":]"
           return $ Class c
        )
 <|>
   try (do
           s <- collElem
           symbol "-"
           t <- collElem
           return (Range s t) <?> "full range"
        )
 <|>
   try (do
           s <- collElem
           symbol "-"
           return (RangeStart s) <?> "starting range"
        )
 <|>
   (collElem >>= return . CE)

-----------------------------------------------------------------------------
---------- R U N N I N G ---------------

run :: Parser ERE -> String -> IO ERE
run p s = case parse p "test" s of
   Left e -> do
                putStr "parse error at "
                print e
                return undefined
   Right x -> return x

-- main function
parser :: String -> Either ParseError ERE
parser = parse ere "test"

test :: String -> IO ERE
test = run ere

---------------------------------------------------------
-- TESTING

test1, test2, test3 :: IO ERE

test1 = test "[a-zA-Z_][a-zA-Z0-9_]*" -- identifier (Python)

test2 = test "0(x|X)[0-9a-fA-F]+" -- hex integers (Python)

test3 = test "+=|-=|*=|/=|%=|**=|>>=|<<=|&=|^=|\\\\=" -- some operators
