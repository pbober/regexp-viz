-- regexp-viz - LPP 2010
-- Piotr Bober
-- Graph generator for DFA (deterministic finite automata)

module Graph where

-- my modules
import DFA (DFA(..))
-- system modules
import Data.List (sort, groupBy)

-- GRAPH GENERATOR

-- create DOT code for the automaton
dfa_to_graph :: DFA -> String
dfa_to_graph (DFA ab st q0 acc del) =
   dot_heading q0 ++
   unlines (map (\q -> show q ++ " [peripheries=2];") (filter acc st)) ++
   unlines (concatMap edges st) ++
   "}\n"
   where
      reach q = map ((\(x,y) -> (head x,y)) . unzip) $ groupBy (\(x,_) (y,_) -> x==y) $ sort $ zip (map (del q) ab) ab
      edges q = map (\(q',a) -> show q ++ " -> " ++ show q' ++ " [label=\"" ++ escape a ++ "\"];") $ reach q
      escape "" = ""
      escape (x:xs)
         | x == '\\' = '\\' : '\\' : escape xs
         | x == '\"' = '\\' : '\"' : escape xs
         | otherwise = x : escape xs

-- create DOT code for the automaton with colored states and edges
dfa_to_graph_color :: DFA -> [Int] -> String
dfa_to_graph_color (DFA ab st q0 acc del) color =
   dot_heading q0 ++
   unlines (map (\q -> show q ++ params q) st) ++
   unlines (concatMap edges st) ++
   "}\n"
   where
      params q
         | acc q =
            if q `elem` color
               then " [peripheries=2, color=\"red\"];"
               else " [peripheries=2];"
         | q `elem` color = " [color=\"red\"];"
         | otherwise = " [color=\"black\"];"
      pairs [] = []
      pairs (x:xs@(y:_)) = (x,y) : pairs xs
      pairs _ = []
      reach q = map ((\(x,y) -> (head x,y)) . unzip) $ groupBy (\(x,_) (y,_) -> x==y) $ sort $ zip (map (del q) ab) ab
      edges q = map (\(q',a) -> show q ++ " -> " ++ show q' ++ " [label=\"" ++ escape a ++ (if (q,q') `elem` pairs color then "\", color=\"red\", fontcolor=\"red" else "") ++ "\"];") $ reach q
      escape "" = ""
      escape (x:xs)
         | x == '\\' = '\\' : '\\' : escape xs
         | x == '\"' = '\\' : '\"' : escape xs
         | otherwise = x : escape xs

-- DOT file heading
dot_heading :: Int -> String
dot_heading q0 =
   "digraph Test1 {\n" ++
   "size=\"8.33,6.25\"\n" ++
   "ratio=fill\n" ++
   "nodesep=equally\n" ++
   "ranksep=equally\n" ++
   "rankdir=LR\n" ++
   "init [shape=\"plaintext\",label=\"\"];\ninit -> " ++ show q0 ++ ";\n"

