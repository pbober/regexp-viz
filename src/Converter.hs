-- regexp-viz - LPP 2010
-- Piotr Bober
-- regular expression AST converter (ERE -> simple regexp)

module Converter where

-- my modules
import Parser (ERE(..), ERE_Dup(..), BracketExpression(..), SingleExpression(..))
-- system modules
import Data.List (foldl', foldl1', (\\))
import Data.Char (chr, ord)

-- regular expressions
data RegExp = Epsilon
            | Atom Char
            | Kleene RegExp
            | RegExp :+: RegExp
            | RegExp :*: RegExp
   deriving (Show, Eq)

-- operators
infixl 6 :+:
infixl 7 :*:

-- converter
convert :: ERE -> RegExp
convert e =
   case e of
      Alt e1 e2      -> convert e1 :+: convert e2
      Concat e1 e2   -> convert e1 :*: convert e2
      OrdChar s      -> Atom s
      QuotedChar s   -> Atom s
      Dot            -> anything
      Bracket e'     -> bracket e'
      Dup d e'       ->
         case d of
            Star                 -> Kleene $ convert e'
            Plus                 -> e'' :+: Kleene e''
               where e'' = convert e'
            QMark                -> Epsilon :+: convert e'
            DupCount a           -> copy a $ convert e'
            DupCountCommaSep a b -> foldl' (\x y -> copy y e'' :+: x) (copy b e'') ((b-1) `downTo` a)
               where e'' = convert e'

copy :: Integer -> RegExp -> RegExp
copy 0 _ = Epsilon
copy 1 e = e
copy n e = e :*: copy (n-1) e

anything_list :: [RegExp]
anything_list = map (Atom . chr) [0..127]

anything :: RegExp
anything = foldl1' (:+:) anything_list

bracket :: BracketExpression -> RegExp
bracket (Matching bl) = foldl1' (:+:) $ bracket_to_list bl
bracket (NonMatching bl) = foldl1' (:+:) $ anything_list \\ bracket_to_list bl

-- make a list from an expression term list
bracket_to_list :: [SingleExpression] -> [RegExp]
bracket_to_list = concatMap single

single :: SingleExpression -> [RegExp]
single (Class cc) = class_to_list cc
single (CE ce) = [Atom ce]
single (Range start end) = map (Atom . chr) [ord start .. ord end]
single (RangeStart start) = map (Atom . chr) [ord start .. 127]

class_to_list :: String -> [RegExp]
class_to_list "alnum" = map Atom "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
class_to_list "alpha" = map Atom "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
class_to_list "blank" = map Atom " \t"
class_to_list "cntrl" = map (Atom . chr) $ [0..31]++[127]
class_to_list "digit" = map Atom "0123456789"
class_to_list "graph" = map (Atom . chr) [33..126]
class_to_list "lower" = map Atom "abcdefghijklmnopqrstuvwxyz"
class_to_list "print" = map (Atom . chr) [32..126]
class_to_list "punct" = map Atom "][!\"#$%&'()*+,./:;<=>?@\\^_`{|}~-"
class_to_list "space" = map Atom " \t\r\n\v\f"
class_to_list "upper" = map Atom "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
class_to_list "xdigit" = map Atom "0123456789ABCDEFabcdef"
class_to_list _ = []

-- simplifier

-- TODO simplify the resulting regular expression


-- auxiliary functions
downTo :: (Num a, Ord a) => a -> a -> [a]
x `downTo` y
   | x < y = []
   | otherwise = x : downTo (x-1) y
