all:
	ghc -O2 -Wall -fno-warn-unused-do-bind --make -o regexp-viz src/*.hs

clean:
	rm -f regexp-viz
	rm -f graph.dot graph.png
	rm -f *.hi *.o
	rm -f src/*.hi src/*.o
	rm -f src/Parser/*.hi src/Parser/*.o
	rm -f src/DFA/*.hi src/DFA/*.o
	rm -f src/Graph/*.hi src/Graph/*.o

