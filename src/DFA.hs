-- regexp-viz - LPP 2010
-- Piotr Bober
-- Regular expression to DFA (deterministic finite automaton) converter

module DFA where

-- my modules
import Converter (RegExp(..))
-- system modules
import Data.List (sort, nub, (\\), subsequences, findIndex, find)

-- DATA STRUCTURES

-- DFA - deterministic finite automaton
data DFA = DFA {
   alphabet :: [Char],
   states :: [Int],
   init :: Int,
   accept :: (Int -> Bool),
   delta :: (Int -> Char -> Int)}

instance Show DFA where
   show (DFA ab st q0 acc del) =
      "DFA:" ++
      "\nalphabet: " ++ show ab ++
      "\nstates: " ++ show st ++
      "\ninit: " ++ show q0 ++
      "\naccept: " ++ show (filter acc st) ++
      "\ndelta: " ++ show (zip st (map (\q -> zip ab (map (del q) ab)) st))

min_dfa :: DFA
min_dfa = DFA [] [] 0 (const False) (const $ const 0)

-- NDFA - non-deterministic finite automaton
data NDFA = NDFA {
   alphabet_n :: [Char],
   states_n :: [Int],
   init_n :: Int,
   accept_n :: (Int -> Bool),
   delta_n :: (Int -> Char -> [Int])}

instance Show NDFA where
   show (NDFA ab st q0 acc del) =
      "NDFA:" ++
      "\nalphabet: " ++ show ab ++
      "\nstates: " ++ show st ++
      "\ninit: " ++ show q0 ++
      "\naccept: " ++ show (filter acc st) ++
      "\ndelta: " ++ show (zip st (map (\q -> zip ab (map (del q) ab)) st))

-- REGEXP -> DFA CONVERSION

-- main conversion function
regexp_to_dfa :: RegExp -> DFA
regexp_to_dfa = optimize . ndfa_to_dfa . regexp_to_ndfa

-- first step: regexp -> ndfa
regexp_to_ndfa :: RegExp -> NDFA
regexp_to_ndfa re =
   case re of
      Epsilon -> NDFA [] [0] 0 (==0) (const $ const [])
      Atom c -> NDFA [c] [0,1] 0 (\q -> if q == 1 then True else False) del
         where
            del 0 a | a == c = [1]
            del _ _ = []
      Kleene re' -> NDFA (alphabet_n ndfa) (states_n ndfa) q0 acc del
         where
            ndfa = regexp_to_ndfa re'
            q0 = init_n ndfa
            acc q = if q == q0 then True else accept_n ndfa q
            delta' = delta_n ndfa
            del q a | accept_n ndfa q = delta' q a ++ delta' q0 a
            del q a = delta' q a
      re1 :+: Atom c -> NDFA ab st q0 acc del
         where
            ndfa1 = regexp_to_ndfa re1
            st1 = states_n ndfa1
            acc1 = accept_n ndfa1
            del1 = delta_n ndfa1
            ab = sort $ nub $ c : alphabet_n ndfa1
            one_step_accept' = filter acc1 $ nub $ concatMap (del1 q0) ab
            one_step_accept = filter (\q -> null $ concatMap (del1 q) ab) one_step_accept'
            st = case one_step_accept of
               [] ->
                  st1 ++ [maximum st1 + 1]
               _ ->
                  st1
            q0 = init_n ndfa1
            acc = case one_step_accept of
               [] ->
                  \q -> q == maximum st1 + 1 || acc1 q
               _ ->
                  acc1
            del = case one_step_accept of
               [] ->
                  \q a -> if q == q0 && a == c
                     then del1 q0 c ++ [maximum st1 + 1]
                     else del1 q a
               (x:_) ->
                  \q a -> if q == q0 && a == c
                     then del1 q0 c ++ [x]
                     else del1 q a
      Atom c :+: re1-> NDFA ab st q0 acc del
         where
            ndfa1 = regexp_to_ndfa re1
            st1 = states_n ndfa1
            acc1 = accept_n ndfa1
            del1 = delta_n ndfa1
            ab = sort $ nub $ c : alphabet_n ndfa1
            one_step_accept' = filter acc1 $ nub $ concatMap (del1 q0) ab
            one_step_accept = filter (\q -> null $ concatMap (del1 q) ab) one_step_accept'
            st = case one_step_accept of
               [] ->
                  st1 ++ [maximum st1 + 1]
               _ ->
                  st1
            q0 = init_n ndfa1
            acc = case one_step_accept of
               [] ->
                  \q -> q == maximum st1 + 1 || acc1 q
               _ ->
                  acc1
            del = case one_step_accept of
               [] ->
                  \q a -> if q == q0 && a == c
                     then del1 q0 c ++ [maximum st1 + 1]
                     else del1 q a
               (x:_) ->
                  \q a -> if q == q0 && a == c
                     then del1 q0 c ++ [x]
                     else del1 q a
      re1 :+: re2 -> NDFA ab st q0 acc del
         where
            ndfa1 = regexp_to_ndfa re1
            ndfa2 = regexp_to_ndfa re2
            q0 = init_n ndfa1
            ab = sort $ nub $ (alphabet_n ndfa1) ++ (alphabet_n ndfa2)
            st1 = states_n ndfa1
            st2 = states_n ndfa2 \\ [init_n ndfa2]
            offset = maximum st1
            st = st1 ++ (map (+ offset) st2)
            acc q = if accept_n ndfa1 q then True else accept_n ndfa2 (q - offset)
            delta1 = delta_n ndfa1
            delta2 = delta_n ndfa2
            del q a | q == q0 = delta1 q a ++ map (+ offset) (delta2 (init_n ndfa2) a)
            del q a | q `elem` st1 = delta1 q a
            del q a = map (+ offset) (delta2 (q - offset) a)
      re1 :*: re2 -> NDFA ab st (init_n ndfa1) acc del
         where
            ndfa1 = regexp_to_ndfa re1
            ndfa2 = regexp_to_ndfa re2
            ab = sort $ nub $ (alphabet_n ndfa1) ++ (alphabet_n ndfa2)
            st1 = states_n ndfa1
            st2 = states_n ndfa2 \\ [init_n ndfa2]
            offset = maximum st1
            st = st1 ++ (map (+ offset) st2)
            acc q =
               if accept_n ndfa2 (init_n ndfa2)
                  then (if q `elem` st1 then accept_n ndfa1 q else accept_n ndfa2 (q - offset))
                  else accept_n ndfa2 (q - offset)
            delta1 = delta_n ndfa1
            delta2 = delta_n ndfa2
            del q a | accept_n ndfa1 q = delta1 q a ++ map (+ offset) (delta2 (init_n ndfa2) a)
            del q a | q `elem` st1 = delta1 q a
            del q a = map (+ offset) (delta2 (q - offset) a)

-- second step: determinisation
ndfa_to_dfa :: NDFA -> DFA
ndfa_to_dfa (NDFA ab st q0 acc del) = DFA ab st'' q0'' acc'' del''
   where
      st'  = subsequences st
      q0' = [q0]
      acc' = not . null . filter acc
      del' q a = sort $ nub $ concatMap (flip del a) q
      st'' = [0..(length st' - 1)]
      q0''  = case findIndex (== q0') st' of
         Nothing -> error "ndfa_to_dfa: state not found"
         Just i -> i
      acc'' q = acc' $ st' !! q
      del'' q a = case findIndex (== (del' (st' !! q) a)) st' of
         Nothing -> error "ndfa_to_dfa: state not found"
         Just i -> i

-- third step: optimization
optimize :: DFA -> DFA
optimize (DFA ab _ q0 acc del) = DFA ab st''' q0' acc' del''
   where
      -- leave only reachable states
      st' = aux [q0]
      aux tmp =
         let
            tmp' = nub $ tmp ++ concatMap (\q -> map (del q) ab) tmp
         in
            if length tmp' == length tmp
               then tmp'
               else aux tmp'
      -- remove indistinguishable states -- TODO na razie nie działa
{-      pairs = [(q1,q2) | q1 <- reverse st', q2 <- filter (>q1) st']
      indistinguishable (q1,q2) = reach1 == reach2 && acc q1 == acc q2
         where
            reach1 = map (del q1) ab
            reach2 = map (del q2) ab
      blacklist = filter indistinguishable pairs
      st'' = st' \\ map snd blacklist
      del' q a = case find (\(_,b) -> b == q') blacklist of
         Nothing -> q'
         Just (x,_) -> x
         where
            q' = del q a-}
      st'' = st'
      del' = del
      -- state renaming
      st''' = [0 .. length st'' - 1]
      q0' = case findIndex (==q0) st'' of
         Nothing -> error $ "optimize: state not found while renaming: " ++ show q0
         Just i -> i
      acc' q = acc $ st'' !! q
      del'' q a = case findIndex (== (del' (st'' !! q) a)) st'' of
         Nothing -> error $ "optimize: state not found while renaming: " ++ show (del' (st'' !! q) a)
         Just i -> i

-- TESTING

-- short name for function testing
test :: RegExp -> DFA
test = regexp_to_dfa

-- regexp examples
test1, test2, test3, test4, test5 :: RegExp

test1 = Atom 'a' :+: Atom 'b' :+: Atom 'c'

test2 = Atom 'a' :*: Atom 'b' :+: Atom 'c'

test3 = (Atom 'a' :+: Atom 'b') :*: Kleene (Atom 'c')

test4 = Atom 'a' :+: Atom 'b'

test5 = Atom 'a' :*: Atom 'b' :*: Atom 'c' :*: Atom 'd' :*: Atom 'e' :*: Atom 'f' :*: Atom 'g'

-- tricky example
test6 :: String
test6 = "[ab][xy]?|c"

-- dfa example (for optimization testing)
loop :: DFA
loop = DFA "a" [0,1,2] 2 (==2) del
   where
      del 0 _ = 1
      del 1 _ = 0
      del 2 _ = 2
      del _ _ = undefined
